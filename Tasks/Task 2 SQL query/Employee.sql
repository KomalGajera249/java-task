/*
    Task: Write a query for below SQL statement
        - There is a table A with 5 columns: firstname,lastname, address, city and salary.
        - Write a query to print the distinct salaries from the table without using distinct in the query.   
*/


-- Table structure for table `Employee`
CREATE TABLE `Employee` (
  `empid` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(70) NOT NULL,
  `salary` decimal(10,2) NOT NULL,
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Dumping data for table `Employee`
INSERT INTO `Employee` 
VALUES 
(1,'CHIN YEN','WA','TACOCA','USA',10000.00),
(2,'MIKE PEARL','B BLOCK NICE STREET','TACOCA','SCODD',12000.00),
(3,'GREEN FIELD','AVE','BRISDON','NEW START',11000.00),
(4,'DEWANE PAUL','SPACE','NEW RIVER','UNITED KINGDOM',12000.00),
(5,'MATTS','KNOWN','COMMONS','ADVANCE PHYSICS',13000.00),
(6,'PLANK OTO','TUCSON','TUCSON, AZ MSA','USA',14000.00),
(7,'Yogesh OTO','Vaishnav','Ahmedabad','India',11000.00),
(8,'Vishal','Vishwakarma','rajkot','NZ',14000.00),
(9,'Ajit','Yadav','baroda','UAE',18000.00),
(10,'Tanvi','Thakur','surat','DEL',13000.00);




-- Solution: using Row number
  
  SELECT salary 
  FROM 'Employee' 
  GROUP BY salary;

    -- OUTPUT 
        --salary
        -- 10000.00
        -- 11000.00
        -- 12000.00
        -- 13000.00
        -- 14000.00
        -- 18000.00
