package com.java.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 * The ReverseNumberTest program implements an application that simply verify
 * the reverseNumber function value.
 *
 * @author Komal Gajera
 * @version 1.0
 */
public class ReverseNumberTest {

	// Test cases for the reverse Number
	@Test
	public void reverseNumberTest() throws Exception {
		int reverseValue = ReverseNumber.reverseNumber(2563);
		assertEquals(3652, reverseValue);
	}

	// Test cases for the reverse with in-correct Number
	@Test
	public void reverseNumberWrongTest() throws Exception {
		Assertions.assertThrows(Exception.class, () -> {
			ReverseNumber.reverseNumber(-1);
		});
	}

}
