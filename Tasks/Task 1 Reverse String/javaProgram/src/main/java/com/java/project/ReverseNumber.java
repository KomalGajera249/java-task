package com.java.project;

import java.util.Scanner;

/**
 * The ReverseNumber program implements an application that simply give the
 * output of the reverse number that is given by user.
 *
 * @author JavaDeveloper
 * @version 1.0
 */
public class ReverseNumber {

	/**
	 * This main method if ReverseNumber
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Scanner scanner = new Scanner(System.in);
		System.out.println("enter the value for reverse: ");
		int inputNumber = scanner.nextInt();
		int result = reverseNumber(inputNumber);
		System.out.println("Reverse number is : " + result);
	}

	static int revNumber = 0;
	static int no = 1;

	/**
	 * This method is used to create the
	 * 
	 * @param number in interger value
	 * @return revNumber
	 * @throws Exception
	 */
	static int reverseNumber(int number) throws Exception {
		if (number > 0) {
			reverseNumber(number / 10);
			revNumber += (number % 10) * no;
			no *= 10;
		} else {
			throw new Exception("Negative number are not allow.");
		}
		return revNumber;
	}
}
